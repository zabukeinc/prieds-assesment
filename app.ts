var createError = require('http-errors');
import express, { Application, Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
import 'dotenv/config'
const salesmanRoute = require('./src/features/salesman/producers/routes/salesman.route');
const customerRoute = require('./src/features/customer/producer/routes/customer.route')
const queueRoute = require('./src/features/queue/producer/routes/queue.route')

const app: Application = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const hostname = '127.0.0.1';
const port = 3000;

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// connect db mongoose
mongoose.connect(process.env.MONGO_URI).then((v) => {
  console.log("succesfully connected")
})

app.use('/salesmans', salesmanRoute);
app.use('/customers', customerRoute)
app.use('/queues', queueRoute)

app.listen(port, () => {
  console.log(`Server runnning on ${hostname}:${port}`)
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
})

// error handler
app.use((err: Error & { status: number }, req: Request, res: Response, next: NextFunction) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
