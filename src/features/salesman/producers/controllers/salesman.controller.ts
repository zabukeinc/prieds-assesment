import mongoose from "mongoose";
import { JsonResponseEntity } from "../../../../base/domain/entities/json-response.entity";
import { BaseController } from "../../../../base/producer/controllers/base.controller";
import { SalesmanEntity } from "../../domain/entities/salesman.entity";
import { CreateSalesmanManager } from "../../domain/managers/create-salesman.manager";
import { DeleteSalesmanManager } from "../../domain/managers/delete-salesman.manager";
import { UpdateSalesmanManager } from "../../domain/managers/update-salesman.manager";

export class SalesmanController extends BaseController<SalesmanEntity> {
  constructor(protected model: mongoose.Model<SalesmanEntity>) {
    super(model);
  }

  async index(): Promise<JsonResponseEntity> {
    // await this.model.deleteMany()
    return this.responses.json(200, await this.model.find());
  }

  async show(id: string): Promise<JsonResponseEntity> {
    const data = await this.model.findById({ _id: id });
    if (!data) throw new Error("Salesman not found");

    return this.responses.json(200, data);
  }

  async create(entity: SalesmanEntity): Promise<JsonResponseEntity> {
    const manager = await new CreateSalesmanManager(
      this.model,
      entity
    ).execute();

    return this.responses.json(201, manager);
  }

  async update(
    id: string,
    entity: SalesmanEntity
  ): Promise<JsonResponseEntity> {
    const manager = await new UpdateSalesmanManager(
      this.model,
      id,
      entity
    ).execute();

    return this.responses.json(201, manager);
  }

  async delete(id: string): Promise<JsonResponseEntity> {
    await new DeleteSalesmanManager(this.model, id).execute();

    return this.responses.json(200, "succesfully deleted");
  }
}
