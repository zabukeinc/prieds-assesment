import express from "express";
import { SalesmanModel } from "../../data/models/salesman.model";
import { SalesmanController } from "../controllers/salesman.controller";

const salesmanRoute = express.Router();

const controller = new SalesmanController(new SalesmanModel().buildSchema());

// index
salesmanRoute.get("/", async (req: express.Request, res: express.Response) => {
  try {
    const data = await controller.index();

    res.json(data);
  } catch (err) {
    if (err instanceof Error)
      res.status(err["status"]).json({
        message: err["message"],
      });
  }
});

// get one
salesmanRoute.get(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const { params } = req;

      const data = await controller.show(params.id);

      res.json(data);
    } catch (err) {
      if (err instanceof Error)
        res.status(err["status"]).json({ message: err.message });
    }
  }
);

// create
salesmanRoute.post("/", async (req: express.Request, res: express.Response) => {
  try {
    const data = await controller.create(req.body);

    return res.json(data);
  } catch (err) {
    if (err instanceof Error)
      res.status(err["status"]).json({ message: err.message });
  }
});

// update
salesmanRoute.put(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const data = await controller.update(req.params.id, req.body);
      return res.json(data);
    } catch (err) {
      if (err instanceof Error)
        res.status(err["status"]).json({ message: err.message });
    }
  }
);

// delete
salesmanRoute.delete(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const data = await controller.delete(req.params.id);
      return res.json(data);
    } catch (err) {
      if (err instanceof Error)
        res.status(err["status"]).json({ message: err.message });
    }
  }
);

module.exports = salesmanRoute;
