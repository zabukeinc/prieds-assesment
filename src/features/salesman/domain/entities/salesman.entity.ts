import { BaseEntity } from "../../../../base/domain/entities/base.entity";

export interface SalesmanEntity extends BaseEntity {
  name: string;
  code: string;
}
