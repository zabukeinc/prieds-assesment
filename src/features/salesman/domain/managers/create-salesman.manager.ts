import mongoose from "mongoose";
import { BaseCreateManager } from "../../../../base/domain/managers/base-create.manager";
import { SalesmanEntity } from "../entities/salesman.entity";

export class CreateSalesmanManager extends BaseCreateManager<SalesmanEntity> {
  constructor(
    protected model: mongoose.Model<SalesmanEntity>,
    protected entity: SalesmanEntity
  ) {
    super(model, entity);
  }

  async preProcess(): Promise<SalesmanEntity> {
    return null;
  }

  async afterProcess(id: string): Promise<SalesmanEntity> {
    return await (await this.model.findOne({ _id: id })).populated("salesman");
  }
}
