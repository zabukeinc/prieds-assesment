import mongoose from "mongoose";
import { BaseDeleteManager } from "../../../../base/domain/managers/base-delete.manager";
import { SalesmanEntity } from "../entities/salesman.entity";

export class DeleteSalesmanManager extends BaseDeleteManager<SalesmanEntity> {
  constructor(
    protected model: mongoose.Model<SalesmanEntity>,
    protected id: string
  ) {
    super(model, id);
  }

  async preProcess(): Promise<SalesmanEntity> {
    return null;
  }
}
