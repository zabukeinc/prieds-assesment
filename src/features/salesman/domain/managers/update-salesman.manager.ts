import mongoose from "mongoose";
import { BaseUpdateManager } from "../../../../base/domain/managers/base-update.manager";
import { SalesmanEntity } from "../entities/salesman.entity";

export class UpdateSalesmanManager extends BaseUpdateManager<SalesmanEntity> {
  constructor(
    protected model: mongoose.Model<SalesmanEntity>,
    protected id: string,
    protected entity: SalesmanEntity
  ) {
    super(model, id, entity);
  }

  async preProcess(): Promise<SalesmanEntity> {
    return null;
  }
}
