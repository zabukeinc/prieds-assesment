import { Schema } from "mongoose";
import { field, schema, buildSchema } from "mongoose-schema-decorators";
import { BaseModel } from "../../../../base/data/models/base.model";
import { SalesmanEntity } from "../../domain/entities/salesman.entity";

@schema({
  toJSON: {
    getters: true,
    virtuals: true,
  },
})
export class SalesmanSchema implements SalesmanEntity {
  @field
  name: string;

  @field
  code: string;

  @field
  id: Schema.Types.ObjectId;

  @field(Date)
  created_at: Schema.Types.Date;

  @field(Date)
  updated_at: Schema.Types.Date;

  @field(Date)
  deleted_at: Schema.Types.Date;
}

export class SalesmanModel extends BaseModel<SalesmanEntity> {
  constructor() {
    super("salesmans", buildSchema(SalesmanSchema));
  }
}
