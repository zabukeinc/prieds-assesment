import mongoose from "mongoose";
import { QueueEntity } from "../../entities/queue.entity";

export class ProcessQueueValidator {
  constructor(
    protected model: mongoose.Model<QueueEntity>,
    protected entity: QueueEntity
  ) {}

  async validate(): Promise<void | Error> {
    const currentDate = new Date().toJSON().slice(0, 10);

    const hasInProcessQueue = await this.model.findOne({
      has_passed: false,
      date: currentDate,
      customer: this.entity.customer,
    });

    if (hasInProcessQueue)
      throw new Error(
        "You have to complete previous queue before to start queueing again."
      );
  }
}
