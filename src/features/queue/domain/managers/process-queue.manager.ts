import mongoose from "mongoose";
import { QueueCodeGenerator } from "../../../../helpers/queue-code-generator.helper";
import { QueueEntity } from "../entities/queue.entity";
import { ProcessQueueValidator } from "./validators/process-queue.validator";

export class ProcessQueueManager {
  constructor(
    protected model: mongoose.Model<QueueEntity>,
    protected entity: QueueEntity
  ) {}

  protected currentDate = new Date().toJSON().slice(0, 10);

  async execute(): Promise<void> {
    await this.preProcess();

    await new ProcessQueueValidator(this.model, this.entity).validate();

    await this.passPreviousQueue();

    this.entity.code = await new QueueCodeGenerator(this.model).generate();

    await this.model.create(this.entity);
  }

  protected async preProcess(): Promise<void> {
    this.entity.has_passed = false;
    this.entity.date = this.currentDate as any;
  }

  protected async passPreviousQueue(): Promise<void> {
    await this.model.findOneAndUpdate(
      {
        has_passed: false,
        date: this.currentDate,
      },
      {
        has_passed: true,
      }
    );
  }
}
