import { CustomerEntity } from "../../../customer/domain/entities/customer.entity";

export interface QueueEntity {
  date: Date;
  code: string;
  customer: CustomerEntity;
  has_passed: boolean;
}
