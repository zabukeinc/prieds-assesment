import { Schema } from "mongoose";
import { field, schema, buildSchema } from "mongoose-schema-decorators";
import { BaseModel } from "../../../../base/data/models/base.model";
import { CustomerEntity } from "../../../customer/domain/entities/customer.entity";
import { QueueEntity } from "../../domain/entities/queue.entity";

@schema({
  toJSON: {
    getters: true,
    virtuals: true,
  },
})
export class QueueSchema implements QueueEntity {
  @field
  id: Schema.Types.ObjectId;

  @field()
  code: string;

  @field(Date)
  date: Date;

  @field(Boolean)
  has_passed: boolean;

  @field({ ref: "customers" })
  customer: CustomerEntity;

  @field(Date)
  created_at: Schema.Types.Date;

  @field(Date)
  updated_at: Schema.Types.Date;

  @field(Date)
  deleted_at: Schema.Types.Date;
}

export class QueueModel extends BaseModel<QueueEntity> {
  constructor() {
    super("queues", buildSchema(QueueSchema));
  }
}
