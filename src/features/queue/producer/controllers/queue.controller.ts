import { ParamsDictionary } from "express-serve-static-core";
import mongoose from "mongoose";
import { JsonResponseEntity } from "../../../../base/domain/entities/json-response.entity";
import { JsonResponse } from "../../../../base/helpers/responses";
import { QueueEntity } from "../../domain/entities/queue.entity";
import { ProcessQueueManager } from "../../domain/managers/process-queue.manager";

export class QueueController {
  constructor(protected model: mongoose.Model<QueueEntity>) {}

  protected responses = new JsonResponse();

  async processQueue(entity: QueueEntity): Promise<JsonResponseEntity> {
    await new ProcessQueueManager(this.model, entity).execute();
    return this.responses.json(200, "Process queue success.");
  }

  async index(params: ParamsDictionary): Promise<JsonResponseEntity> {
    const currentParams = {};

    if (params["current_date"] === "true") {
      Object.assign(currentParams, {
        ...currentParams,
        date: new Date().toJSON().slice(0, 10),
      });
    }

    if (params["has_passed"] === "true") {
      Object.assign(currentParams, {
        ...currentParams,
        has_passed: true,
      });
    }

    return this.responses.json(200, await this.model.find(currentParams));
  }

  async showQueueByCustomerId(customerId: string): Promise<JsonResponseEntity> {
    const currentDate = new Date().toJSON().slice(0, 10);

    const queue = await this.model.findOne({
      date: currentDate,
      // customer: {
      //   id: customerId,
      // },
      customer: customerId,
    });

    if (!queue) throw new Error("Queue with specific customer is not found");

    return this.responses.json(200, queue);
  }
}
