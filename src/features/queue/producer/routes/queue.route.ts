import express from "express";
import { QueueModel } from "../../data/models/queue.model";
import { QueueController } from "../controllers/queue.controller";

const queueRoute = express.Router();

const controller = new QueueController(new QueueModel().buildSchema());

// process queue
queueRoute.post("/", async (req: express.Request, res: express.Response) => {
  try {
    const data = await controller.processQueue(req.body);

    res.json(data);
  } catch (err) {
    if (err instanceof Error)
      res.status(404).json({
        message: err["message"],
      });
  }
});

// get queue by customer id
queueRoute.get(
  `/by/customer/:customerId`,
  async (req: express.Request, res: express.Response) => {
    try {
      const { params } = req;

      const data = await controller.showQueueByCustomerId(params.customerId);

      res.json(data);
    } catch (err) {
      if (err instanceof Error) res.status(400).json({ message: err.message });
    }
  }
);

// get all queue
queueRoute.get(`/`, async (req: express.Request, res: express.Response) => {
  try {
    const { params } = req;

    const data = await controller.index(params);

    res.json(data);
  } catch (err) {
    if (err instanceof Error) res.status(400).json({ message: err.message });
  }
});

module.exports = queueRoute;
