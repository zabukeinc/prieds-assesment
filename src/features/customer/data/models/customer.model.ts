import { Schema } from "mongoose";
import { field, schema, buildSchema } from "mongoose-schema-decorators";
import { BaseModel } from "../../../../base/data/models/base.model";
import { SalesmanModel } from "../../../salesman/data/models/salesman.model";
import { CustomerEntity } from "../../domain/entities/customer.entity";

@schema({
  toJSON: {
    getters: true,
    virtuals: true,
  },
})
export class CustomerSchema implements CustomerEntity {
  @field
  id: Schema.Types.ObjectId;

  @field
  name: string;

  @field
  pic: string;

  @field
  remark: string;

  @field(Number)
  npwp: number;

  @field(Number)
  price_category: number;

  @field
  address: string;

  @field
  second_address: string;

  @field(Number)
  contact_no: number;

  @field
  region: string;

  @field
  province: string;

  @field
  city: string;

  @field(Number)
  quota: number;

  @field({ ref: "salesmans" })
  salesman?: SalesmanModel;

  @field(Date)
  created_at: Schema.Types.Date;

  @field(Date)
  updated_at: Schema.Types.Date;

  @field(Date)
  deleted_at: Schema.Types.Date;
}

export class CustomerModel extends BaseModel<CustomerEntity> {
  constructor() {
    super("customers", buildSchema(CustomerSchema));
  }
}
