import mongoose from "mongoose";
import { JsonResponseEntity } from "../../../../base/domain/entities/json-response.entity";
import { BaseController } from "../../../../base/producer/controllers/base.controller";
import { CustomerEntity } from "../../domain/entities/customer.entity";
import { CreateCustomerManager } from "../../domain/managers/create-customer.manager";
import { DeleteCustomerManager } from "../../domain/managers/delete-customer.manager";
import { UpdateCustomerManager } from "../../domain/managers/update-customer.manager";

export class CustomerController extends BaseController<CustomerEntity> {
  constructor(protected model: mongoose.Model<CustomerEntity>) {
    super(model);
  }

  async index(): Promise<JsonResponseEntity> {
    return this.responses.json(200, await this.model.find());
  }

  async show(id: string): Promise<JsonResponseEntity> {
    const data = await this.model.findById({ _id: id });
    if (!data) throw new Error("Customer not found");

    return this.responses.json(200, data);
  }

  async create(entity: CustomerEntity): Promise<JsonResponseEntity> {
    const manager = await new CreateCustomerManager(
      this.model,
      entity
    ).execute();

    return this.responses.json(201, manager);
  }

  async update(
    id: string,
    entity: CustomerEntity
  ): Promise<JsonResponseEntity> {
    const manager = await new UpdateCustomerManager(
      this.model,
      id,
      entity
    ).execute();

    return this.responses.json(201, manager);
  }

  async delete(id: string): Promise<JsonResponseEntity> {
    await new DeleteCustomerManager(this.model, id).execute();

    return this.responses.json(200, "succesfully deleted");
  }
}
