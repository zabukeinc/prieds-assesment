import express from "express";
import { CustomerModel } from "../../data/models/customer.model";
import { CustomerController } from "../controllers/customer.controller";

const customerRoute = express.Router();

const controller = new CustomerController(new CustomerModel().buildSchema());

// index
customerRoute.get("/", async (req: express.Request, res: express.Response) => {
  try {
    const data = await controller.index();

    res.json(data);
  } catch (err) {
    if (err instanceof Error)
      res.status(404).json({
        message: err["message"],
      });
  }
});

// get one
customerRoute.get(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const { params } = req;

      const data = await controller.show(params.id);

      res.json(data);
    } catch (err) {
      if (err instanceof Error) res.status(400).json({ message: err.message });
    }
  }
);

// create
customerRoute.post("/", async (req: express.Request, res: express.Response) => {
  try {
    const data = await controller.create(req.body);

    return res.json(data);
  } catch (err) {
    if (err instanceof Error) res.status(400).json({ message: err.message });
  }
});

// update
customerRoute.put(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const data = await controller.update(req.params.id, req.body);
      return res.json(data);
    } catch (err) {
      if (err instanceof Error) res.status(400).json({ message: err.message });
    }
  }
);

// delete
customerRoute.delete(
  `/:id`,
  async (req: express.Request, res: express.Response) => {
    try {
      const data = await controller.delete(req.params.id);
      return res.json(data);
    } catch (err) {
      if (err instanceof Error) res.status(400).json({ message: err.message });
    }
  }
);

module.exports = customerRoute;
