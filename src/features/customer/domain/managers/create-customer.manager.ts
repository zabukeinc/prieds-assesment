import mongoose from "mongoose";
import { BaseCreateManager } from "../../../../base/domain/managers/base-create.manager";
import { CustomerEntity } from "../entities/customer.entity";

export class CreateCustomerManager extends BaseCreateManager<CustomerEntity> {
  constructor(
    protected model: mongoose.Model<CustomerEntity>,
    protected entity: CustomerEntity
  ) {
    super(model, entity);
  }

  async preProcess(): Promise<CustomerEntity> {
    return null;
  }

  async afterProcess(id: string): Promise<CustomerEntity> {
    return await this.model.findById({ _id: id });
  }
}
