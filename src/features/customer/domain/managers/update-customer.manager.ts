import mongoose from "mongoose";
import { BaseUpdateManager } from "../../../../base/domain/managers/base-update.manager";
import { CustomerEntity } from "../entities/customer.entity";

export class UpdateCustomerManager extends BaseUpdateManager<CustomerEntity> {
  constructor(
    protected model: mongoose.Model<CustomerEntity>,
    protected id: string,
    protected entity: CustomerEntity
  ) {
    super(model, id, entity);
  }

  async preProcess(): Promise<CustomerEntity> {
    return null;
  }
}
