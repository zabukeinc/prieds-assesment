import mongoose from "mongoose";
import { BaseDeleteManager } from "../../../../base/domain/managers/base-delete.manager";
import { CustomerEntity } from "../entities/customer.entity";

export class DeleteCustomerManager extends BaseDeleteManager<CustomerEntity> {
  constructor(
    protected model: mongoose.Model<CustomerEntity>,
    protected id: string
  ) {
    super(model, id);
  }

  async preProcess(): Promise<CustomerEntity> {
    return null;
  }
}
