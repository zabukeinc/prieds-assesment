export interface CustomerEntity {
  name: string;
  pic: string;
  remark: string;
  npwp: number;
  price_category: number;
  address: string;
  second_address: string;
  contact_no: number;
  region: string;
  province: string;
  city: string;
  quota: number;
}
