export interface JsonResponseEntity {
  status: any;
  error: Error;
  data: string;
}
