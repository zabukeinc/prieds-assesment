import { Schema } from "mongoose";

export interface BaseEntity {
  id: Schema.Types.ObjectId;
  created_at: Schema.Types.Date;
  updated_at: Schema.Types.Date;
  deleted_at: Schema.Types.Date;
}
