import mongoose from "mongoose";

export abstract class BaseCreateManager<Entity> {
  constructor(
    protected model: mongoose.Model<Entity>,
    protected entity: Entity
  ) {}

  protected modifiedEntity: Entity;

  async execute(): Promise<Entity> {
    await this.preProcess();

    const saved = await this.model.create(this.modifiedEntity || this.entity);

    return await this.afterProcess(saved._id);
  }

  abstract preProcess(): Promise<Entity>;
  abstract afterProcess(id: string): Promise<Entity>;
}
