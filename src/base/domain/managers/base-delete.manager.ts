import mongoose from "mongoose";

export abstract class BaseDeleteManager<Entity> {
  constructor(protected model: mongoose.Model<Entity>, protected id: string) {}

  async execute(): Promise<void> {
    await this.preProcess();
    await this.process();
  }

  abstract preProcess(): Promise<Entity>;

  protected async process(): Promise<void> {
    await this.model.deleteOne({ _id: this.id as never });
  }
}
