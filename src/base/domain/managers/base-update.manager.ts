import mongoose from "mongoose";

export abstract class BaseUpdateManager<Entity> {
  constructor(
    protected model: mongoose.Model<Entity>,
    protected id: string,
    protected entity: Entity
  ) {}

  protected modifiedEntity: Entity;

  async execute(): Promise<Entity> {
    await this.preProcess();

    await this.model.updateOne({ _id: this.id as never }, { ...this.entity });

    return await this.model.findOne({ _id: this.id as never });
  }

  abstract preProcess(): Promise<Entity>;
}
