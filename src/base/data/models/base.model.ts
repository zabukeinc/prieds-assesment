import mongoose from "mongoose";

export class BaseModel<Entity> {
  constructor(
    protected schemaName: string,
    protected schema: mongoose.Schema
  ) {}

  buildSchema(): mongoose.Model<Entity> {
    return mongoose.model<Entity>(this.schemaName, this.schema);
  }
}
