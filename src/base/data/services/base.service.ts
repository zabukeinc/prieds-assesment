import mongoose from "mongoose";

export class BaseService<Entity> {
  constructor(protected model: mongoose.Model<Entity>) {}

  async getAll(): Promise<mongoose.HydratedDocument<Entity>[]> {
    return await this.model.find();
  }

  async getOne(id: string): Promise<mongoose.HydratedDocument<Entity>> {
    return await this.model.findById(id);
  }

  async create(entity: Entity): Promise<mongoose.HydratedDocument<Entity>> {
    const saved = await this.model.create(entity);

    if (!saved) throw new Error("Unable to save");
    return await this.getOne(saved.id);
  }

  async update(
    id: string,
    entity: Entity
  ): Promise<mongoose.HydratedDocument<Entity>> {
    const updated = await this.model.updateOne(
      { id: id as never },
      { ...entity }
    );

    if (!updated || updated.modifiedCount === 0)
      throw new Error("Unable to update");
    return await this.getOne(id);
  }

  async delete(id: string): Promise<mongoose.HydratedDocument<void>> {
    await this.model.deleteOne({ id: id as never });
  }
}
