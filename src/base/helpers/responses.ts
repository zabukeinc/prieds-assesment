import { JsonResponseEntity } from "../domain/entities/json-response.entity";

export class JsonResponse {
  json(status: number, data: any, error?: any): JsonResponseEntity {
    return {
      status,
      data,
      error,
    };
  }
}
