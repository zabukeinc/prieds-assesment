import mongoose from "mongoose";
import { JsonResponseEntity } from "../../domain/entities/json-response.entity";
import { JsonResponse } from "../../helpers/responses";

export abstract class BaseController<Entity> {
  constructor(protected model: mongoose.Model<Entity>) {}

  public responses = new JsonResponse();

  abstract show(id: string): Promise<JsonResponseEntity>;
  abstract create(entity: Entity): Promise<JsonResponseEntity>;
  abstract update(id: string, entity: Entity): Promise<JsonResponseEntity>;
  abstract delete(id: string): Promise<JsonResponseEntity>;
}
