import mongoose from "mongoose";
import { QueueEntity } from "../features/queue/domain/entities/queue.entity";

export class QueueCodeGenerator {
  constructor(protected model: mongoose.Model<QueueEntity>) {}

  async generate(): Promise<string> {
    const currentDate = new Date().toJSON().slice(0, 10);
    const counted = await this.model.find({
      has_passed: true,
      date: currentDate,
    });

    const total = counted.length;

    return this.format(total);
  }

  protected format(total: number): string {
    if (!total) total = 0;
    return "A" + total.toString().padStart(3, "0");
  }
}
